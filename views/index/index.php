<div class="row">
  <div class="col-lg-12">
    <h3>What's New Today</h3>
  </div>
  <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-send"></i>
      </div>
      <div class="count">3</div>
      <h3>Total Recieved</h3>
    </div>
  </div>
  <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-cogs"></i>
      </div>
      <div class="count">179</div>
      <h3>In Progress</h3>
    </div>
  </div>
  <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check"></i>
      </div>
      <div class="count">179</div>
      <h3>Completed</h3>
    </div>
  </div>
  <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-clock-o"></i>
      </div>
      <div class="count">179</div>
      <h3>Pending</h3>
    </div>
  </div>
   <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-close"></i>
      </div>
      <div class="count">179</div>
      <h3>Rejected</h3>
    </div>
  </div>
   <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-thumbs-down"></i>
      </div>
      <div class="count">179</div>
      <h3>Cancelled</h3>
    </div>
  </div>
</div>